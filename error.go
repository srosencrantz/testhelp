/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package testhelp

import (
	"testing"
)

// FailError causes a testing fail if err is not nil
func FailError(t *testing.T, err error, msg string) {
	if err != nil {
		t.Errorf("%v: %v", msg, err)
	}
}

// FatalError causes a testing fail if err is not nil
func FatalError(t *testing.T, err error, msg string) {
	if err != nil {
		t.Fatalf("%v: %v", msg, err)
	}
}
