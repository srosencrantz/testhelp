/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package compare

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"reflect"
	"strconv"
	"strings"

	"bitbucket.org/srosencrantz/errcat"
)

// DesiredSignificantDigits is the number of significant digits that will be compared by FloatTest, FloatVectorTest, FloatMatrixTest, and Float3DMatrixTest.
var DesiredSignificantDigits = 14

// DesiredTol is the tolerance such that 1-(expected/actual) < DesiredTol will guarentee that DesiredSignificantDigits of expected and actual match.
var DesiredTol = 1 * math.Pow10(-1*DesiredSignificantDigits)

// SignificantDigitComp compares significant digits of a and b and returns the number of matching digits with a max of 17.
// If a and b both equal zero it returns a maximum value of 17.
// If the exponents don't match it returns 0.
// If the exponents are greator than 307 or less than -307 it returns the maximum value of 17.
// Otherwise returns the number of matching mantissa digits.
func SignificantDigitComp(a, b float64) (matchSig int) {
	maxPrec := 17

	if a == float64(0.0) && b == float64(0.0) {
		return maxPrec
	}

	aStr := strconv.FormatFloat(a, 'e', maxPrec, 64)
	aParts := strings.Split(aStr, "e")
	aParts[0] = strings.Replace(aParts[0], ".", "", 1)

	bStr := strconv.FormatFloat(b, 'e', maxPrec, 64)
	bParts := strings.Split(bStr, "e")
	bParts[0] = strings.Replace(bParts[0], ".", "", 1)

	// compare exponents
	if aParts[1] != bParts[1] {
		return 0
	}

	expVal, _ := strconv.Atoi(aParts[1])
	if expVal < -307 || expVal > 307 {
		return maxPrec
	}

	// compare mantissa
	var i int
	for i = 0; i < len(aParts[0]); i++ {
		if aParts[0][i] != bParts[0][i] {
			break
		}
	}
	return i
}

// IsFatal checks for an err and runs log.Fatal with the message and the err if the err is not nil
func IsFatal(err error, msg string) {
	if err != nil {
		log.Fatalf(msg+": %v", err)
	}
}

// FloatCheck compares two float64 numbers and returns a false if they are not equal to the DesiredSignificantDigits.
func FloatCheck(a, b float64) (match bool, err error) {
	if math.IsNaN(a) || math.IsInf(a, 0) {
		err = errcat.AppendErrStr(err, fmt.Sprintf("FloatCheck NaN/Inf a: %.20e", a))
		return false, err
	}

	if math.IsNaN(b) || math.IsInf(b, 0) {
		err = errcat.AppendErrStr(err, fmt.Sprintf("FloatCheck NaN/Inf b: %.20e", b))
		return false, err
	}

	matchSig := SignificantDigitComp(a, b)
	if matchSig < DesiredSignificantDigits {
		err = errcat.AppendInfoStr(err, fmt.Sprintf("Matched %d digits a = %.20e b = %.20e \n", matchSig, a, b))
		return false, err
	}

	return true, nil
}

// FprintFormatedVector prints a  []float64 vector V to a bufio.Writer (fW) with the specified name, column width (format), index header (iheader) and column headers (header).
func FprintFormatedVector(fW *bufio.Writer, V []float64, name string, format int, header, iheader string) {
	strFormat, intFormat, floatFormat := getFormatStrs(format)
	fmt.Fprintf(fW, "%s : \n", name)
	fmt.Fprintf(fW, strFormat+", "+strFormat+", \n", iheader, header)
	for i := range V {
		fmt.Fprintf(fW, intFormat+", "+floatFormat+", \n", i, V[i])
	}
	fmt.Fprintf(fW, "\n\n")
}

// getFormatStrs generates a string, integer, and float format string given a field width (length).
func getFormatStrs(length int) (strFormat, intFormat, floatFormat string) {
	strFormat = fmt.Sprintf("%%%ds", length+7)
	intFormat = fmt.Sprintf("%%%dd", length+7)
	floatFormat = fmt.Sprintf("%%%d.%de", length+7, length)
	return strFormat, intFormat, floatFormat
}

// IsNil returns true if an interface value is nil
func IsNil(a interface{}) bool {
	defer func() { recover() }()
	return a == nil || reflect.ValueOf(a).IsNil()
}

// CopyMatrix creates a new 2d float64 matrix with the same elements as the original
func CopyMatrix(origM [][]float64) (newM [][]float64) {
	newM = make([][]float64, len(origM))
	for i := range origM {
		newM[i] = make([]float64, len(origM[i]))
		copy(newM[i], origM[i])
	}
	return newM
}

// ProdABv - matrix-vector multiplication cv = A*bv
func ProdABv(a [][]float64, bv []float64, I int) (cv []float64) {
	cv = make([]float64, I)
	for i := 0; i < I; i++ {
		for j := 0; j < I; j++ {
			cv[i] += a[i][j] * bv[j]
		}
	}
	return cv
}

// PrintMatrix prints a 2d float64 matrix in Matlab format
func PrintMatrix(M [][]float64) {
	for icount, i := range M {
		if icount == 0 {
			fmt.Printf("[")
		} else {
			fmt.Printf(" ")
		}
		for jcount, j := range i {
			fmt.Printf("%10.4f", j)
			if jcount < len(i)-1 {
				fmt.Printf(", ")
			} else {
				fmt.Printf(" ")
			}
		}
		if icount < len(M)-1 {
			fmt.Printf(";\n")
		} else {
			fmt.Printf("]\n")
		}
	}
}

// PrintIntMatrix prints a 2d int matrix in Matlab format
func PrintIntMatrix(M [][]int) {
	for icount, i := range M {
		if icount == 0 {
			fmt.Printf("[")
		} else {
			fmt.Printf(" ")
		}
		for jcount, j := range i {
			fmt.Printf("%d", j)
			if jcount < len(i)-1 {
				fmt.Printf(", ")
			} else {
				fmt.Printf(" ")
			}
		}
		if icount < len(M)-1 {
			fmt.Printf(";\n")
		} else {
			fmt.Printf("]\n")
		}
	}
}

// PrintFormatMatrix prints a 2d float64 matrix (M) in Matlab format with the specified number format string (format) equal to a variable named with the string "name"
func PrintFormatMatrix(M [][]float64, format, name string) {
	fmt.Println(name, ": ")
	for icount, i := range M {
		if icount == 0 {
			fmt.Printf("[")
		} else {
			fmt.Printf(" ")
		}
		for jcount, j := range i {
			fmt.Printf(format+" ", j)
			if jcount < len(i)-1 {
				fmt.Printf(", ")
			} else {
				fmt.Printf(" ")
			}
		}
		if icount < len(M)-1 {
			fmt.Printf(";\n")
		} else {
			fmt.Printf("]\n")
		}
	}
}

// FprintFormatedMatrix prints a 2d float64 matrix M to a bufio.Writer (fW) with the specified name, column width (format),  index header (iheader) and column headers (header).
func FprintFormatedMatrix(fW *bufio.Writer, M [][]float64, name string, format int, header []string, iheader string) {
	strFormat, intFormat, floatFormat := getFormatStrs(format)
	fmt.Fprintf(fW, "%s : \n", name)
	fmt.Fprintf(fW, strFormat+", ", iheader)
	for i := range header {
		fmt.Fprintf(fW, strFormat+", ", header[i])
	}
	fmt.Fprintf(fW, "\n")
	for i := range M {
		fmt.Fprintf(fW, intFormat+", ", i)
		for j := range M[i] {
			fmt.Fprintf(fW, floatFormat+", ", M[i][j])
		}
		fmt.Fprintf(fW, "\n")
	}
	fmt.Fprintf(fW, "\n\n")
}
