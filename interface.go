/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package testhelp

import (
	"reflect"
	"testing"

	"bitbucket.org/srosencrantz/testhelp/compare"
)

// CompareData performs a DeepEqual on two interfaces and returns a testing error if they are not equal.
func CompareData(t *testing.T, name string, val, expected interface{}) {
	DataIsNil(t, name, "val", val)
	DataIsNil(t, name, "expected", expected)
	if !reflect.DeepEqual(val, expected) {
		t.Errorf(" %s : Expected: \n %#+v \n got: \n %#+v \n", name, expected, val)
	}
}

// DataIsNil returns true and a testing error if interface A's value is nil
func DataIsNil(t *testing.T, label, name string, A interface{}) {
	if compare.IsNil(A) {
		t.Errorf(" %s %s is nil", label, name)
	}
}
