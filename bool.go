/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package testhelp

import (
	"fmt"
	"testing"
)

// BoolTestR returns a testing error if the two booleans are not equal.
func BoolTestR(t *testing.T, name, aStr, bStr string, a, b bool, firstErr *bool) {
	if a != b {
		if *firstErr {
			t.Errorf("\n\n%s: \n", name)
			*firstErr = false
		}
		t.Errorf("Expected: %s = %v Got: %s = %v \n", aStr, a, bStr, b)
	}
}

// BoolTest is a wrapper for BoolTestR that sets some default values for its inputs.
func BoolTest(t *testing.T, name string, a, b bool) {
	firstErr := true
	BoolTestR(t, name, "a", "b", a, b, &firstErr)
}

// BoolVectorTest returns a testing error if the two boolean vectors are not equal.
func BoolVectorTest(t *testing.T, name string, a, b []bool) {
	firstErr := true
	LenTest(t, name, "a", "b", len(a), len(b), &firstErr)
	for i := 0; i < len(a); i++ {
		BoolTestR(t, name, fmt.Sprintf("a[%d]", i), fmt.Sprintf("b[%d]", i), a[i], b[i], &firstErr)
	}
}
