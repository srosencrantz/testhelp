/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package testhelp

import (
	"fmt"
	"math"
	"testing"

	"bitbucket.org/srosencrantz/testhelp/compare"
)

// FloatTestR compares two float64 numbers and returns a testing error if they are not equal to the DesiredSignificantDigits.
func FloatTestR(t *testing.T, label, aStr, bStr string, a, b float64, firstErr *bool) {
	if math.IsNaN(a) || math.IsInf(a, 0) {
		if *firstErr {
			t.Errorf("\n\n%s: \n", label)
			*firstErr = false
		}
		t.Errorf("%s : %.20e \n", aStr, a)
	}

	if math.IsNaN(b) || math.IsInf(b, 0) {
		if *firstErr {
			t.Errorf("\n\n%s: \n", label)
			*firstErr = false
		}
		t.Errorf("%s : %.20e \n", bStr, b)
	}

	matchSig := compare.SignificantDigitComp(a, b)
	if matchSig < compare.DesiredSignificantDigits {
		if *firstErr {
			t.Errorf("\n\n%s: \n", label)
			*firstErr = false
		}
		t.Errorf("Matched %d digits Expected: %s = %.20e Got: %s = %.20e \n", matchSig, aStr, a, bStr, b)
	}
}

// FloatTest is a brief wrapper around FloatTest
func FloatTest(t *testing.T, label string, val, expected float64) {
	firstErr := true
	FloatTestR(t, label, "Value", "Value", expected, val, &firstErr)
}

// Float2VecTest compares two 2d vectors
func Float2VecTest(t *testing.T, label string, val, expected [2]float64) {
	firstErr := true
	for i := 0; i < 2; i++ {
		FloatTestR(t, label, fmt.Sprintf("Value[%d]", i), fmt.Sprintf("Value[%d]", i), expected[i], val[i], &firstErr)
	}
}

// Float3VecTest compares two 3d vectors
func Float3VecTest(t *testing.T, label string, val, expected [3]float64) {
	firstErr := true
	for i := 0; i < 3; i++ {
		FloatTestR(t, label, fmt.Sprintf("Value[%d]", i), fmt.Sprintf("Value[%d]", i), expected[i], val[i], &firstErr)
	}
}

// FloatVecTest compares two []float64 and returns a testing error if they are not equal to the DesiredSignificantDigits.
func FloatVecTest(t *testing.T, label string, val, expected []float64) {
	firstErr := true
	LenTest(t, label, "Val", "Expected", len(val), len(expected), &firstErr)
	for i := 0; i < len(val); i++ {
		FloatTestR(t, label, fmt.Sprintf("a[%d]", i), fmt.Sprintf("b[%d]", i), expected[i], val[i], &firstErr)
	}
}

// ApproxVectorTest is a wrapper around FloatVectorTest that ignores tol.  This is depricated.
// func ApproxVectorTest(a, b []float64, tol float64, name string, t *testing.T) {
// 	FloatVectorTest(a, b, name, t)
// }

// ApproxFloatTest is a wrapper around FloatTest that ignores tol.  This is depricated.
// func ApproxFloatTest(a, b, tol float64, name string, t *testing.T) {
// 	firstErr := true
// 	FloatTest(a, b, "a", "b", name, &firstErr, t)
// }

// FloatTestC is a brief wrapper around FloatTest
// func FloatTestC(t *testing.T, label string, val, expected float64) {
// 	firstErr := true
// 	if math.IsNaN(expected) || math.IsInf(expected, 0) {
// 		if firstErr {
// 			t.Errorf("\n\n%s: \n", label)
// 			firstErr = false
// 		}
// 		t.Errorf("%s : %.20e \n", "Value", expected)
// 	}

// 	if math.IsNaN(val) || math.IsInf(val, 0) {
// 		if firstErr {
// 			t.Errorf("\n\n%s: \n", label)
// 			firstErr = false
// 		}
// 		t.Errorf("%s : %.20e \n", "Value", val)
// 	}

// 	if math.Abs(expected) < compare.DesiredTol {
// 		if math.Abs(val) > compare.DesiredTol {
// 			if firstErr {
// 				t.Errorf("\n\n%s: \n", label)
// 				firstErr = false
// 			}
// 			t.Errorf("Floats didn't match, Expected: %s = %.20e Got: %s = %.20e \n", "Value", expected, "Value", val)
// 		}
// 		return
// 	}

// 	if math.Abs(float64(1)-((expected+compare.DesiredTol)/(val+compare.DesiredTol))) > compare.DesiredTol {
// 		if firstErr {
// 			t.Errorf("\n\n%s: \n", label)
// 			firstErr = false
// 		}
// 		t.Errorf("Floats didn't match, Expected: %s = %.20e Got: %s = %.20e \n", "Value", expected, "Value", val)
// 	}
//}
