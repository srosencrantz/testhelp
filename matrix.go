/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package testhelp

import (
	"fmt"
	"testing"
)

// FloatMatrixTest compares two [][]float64 matricies and returns a testing error if they are not equal to the DesiredSignificantDigits.
func FloatMatrixTest(t *testing.T, label string, val, expected [][]float64) {
	firstErr := true
	LenTest(t, label, "val", "expected", len(val), len(expected), &firstErr)
	for i := 1; i < len(val); i++ {
		LenTest(t, label, fmt.Sprintf("val[%d]", i), "val[0]", len(val[i]), len(val[0]), &firstErr)
		LenTest(t, label, fmt.Sprintf("val[%d]", i), fmt.Sprintf("expected[%d]", i), len(expected[i]), len(expected[i]), &firstErr)
	}

	for i := 0; i < len(val); i++ {
		for j := 0; j < len(val[0]); j++ {
			FloatTestR(t, label, fmt.Sprintf("val[%d][%d]", i, j), fmt.Sprintf("expected[%d][%d]", i, j), val[i][j], expected[i][j], &firstErr)
		}
	}
}

// Float3DMatrixTest compares two [][][]float64 matricies and returns a testing error if they are not equal to the DesiredSignificantDigits.
func Float3DMatrixTest(t *testing.T, name string, a, b [][][]float64) {
	firstErr := true
	LenTest(t, name, "a", "b", len(a), len(b), &firstErr)
	for i := 1; i < len(a); i++ {
		LenTest(t, name, fmt.Sprintf("a[%d]", i), "a[0]", len(a[i]), len(a[0]), &firstErr)
		LenTest(t, name, fmt.Sprintf("a[%d]", i), fmt.Sprintf("b[%d]", i), len(a[i]), len(b[i]), &firstErr)
		for j := 1; j < len(a[0]); j++ {
			LenTest(t, name, fmt.Sprintf("a[%d][%d]", i, j), "a[0][0]", len(a[i][j]), len(a[0][0]), &firstErr)
			LenTest(t, name, fmt.Sprintf("a[%d][%d]", i, j), fmt.Sprintf("b[%d][%d]", i, j), len(a[i][j]), len(b[i][j]), &firstErr)
		}
	}

	for i := 0; i < len(a); i++ {
		for j := 0; j < len(a[0]); j++ {
			for k := 0; k < len(a[0][0]); k++ {
				FloatTestR(t, name, fmt.Sprintf("a[%d][%d][%d]", i, j, k), fmt.Sprintf("b[%d][%d][%d]", i, j, k), a[i][j][k], b[i][j][k], &firstErr)
			}
		}
	}
}

// // ApproxMatrixTest is a wrapper around FloatMatrixTest that ignores tol.  This is depricated.
// func ApproxMatrixTest(a, b [][]float64, tol float64, name string, t *testing.T) {
// 	FloatMatrixTest(a, b, name, t)
// }
