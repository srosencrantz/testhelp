/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package testhelp

import (
	"fmt"
	"testing"
)

// IntTestR returns a testing error if the two ints are not equal.
func IntTestR(t *testing.T, name, aStr, bStr string, a, b int, firstErr *bool) {
	if a != b {
		if *firstErr {
			t.Errorf("\n\n%s: \n", name)
			*firstErr = false
		}
		t.Errorf("Expected: %s = %d Got: %s = %d \n", aStr, a, bStr, b)
	}
}

// IntTest is a wrapper for IntTestR that sets some default values for its inputs.
func IntTest(t *testing.T, name string, val, expected int) {
	firstErr := true
	IntTestR(t, name, "Value", "Value", expected, val, &firstErr)
}

// Int2VecTest compares two 2d vectors
func Int2VecTest(t *testing.T, label string, value, expected [2]int) {
	if value != expected {
		t.Errorf("%s got: %d, expected: %d", label, value, expected)
	}
}

// Int3VecTest compares two 3d vectors
func Int3VecTest(t *testing.T, label string, value, expected [3]int) {
	if value != expected {
		t.Errorf("%s got: %d, expected: %d", label, value, expected)
	}
}

// IntVecTest compares two []int and returns a testing error if they are not equal.
func IntVecTest(t *testing.T, name string, val, expected []int) {
	firstErr := true
	LenTest(t, name, "a", "b", len(val), len(expected), &firstErr)
	for i := 0; i < len(val); i++ {
		IntTestR(t, name, fmt.Sprintf("a[%d]", i), fmt.Sprintf("b[%d]", i), expected[i], val[i], &firstErr)
	}
}
