/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package testhelp

import (
	"testing"
)

// StringTest compares two strings and returns a testing error if they are not equal.
func StringTest(t *testing.T, name string, a, b string) {
	if a != b {
		t.Errorf("\n\n%s: \n", name)
		t.Errorf("%s != %s\n", a, b)
	}
}
